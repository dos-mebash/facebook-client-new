import javax.crypto.spec.IvParameterSpec

import akka.actor.{Actor, ActorSystem}
import akka.event.Logging
import entity._
import org.apache.commons.codec.binary.Base64
import security.{GetDigitalSign, AES, RSA}
import spray.client.pipelining._

import scala.concurrent.Await
import scala.concurrent.duration._
import spray.json._

import scala.util.parsing.json.JSONObject

/**
  * Created by mebin on 12/8/15 for Facebook-Client
  */

class MockUserActor extends Actor {

  import UserJsonProtocol._

  //import UserPostJsonImplicits._

  import spray.httpx.SprayJsonSupport._

  //import DefaultJsonProtocol._


  val mapOfCircleAndAESKey: Map[String, String] = Map[String, String]()

  val r = scala.util.Random
  implicit val system = ActorSystem("facebook-simple-spray-client")
  val log = Logging(system, getClass)

  import system.dispatcher

  private val userParentPath: String = "http://localhost:8080/user/"
  private val circleParentPath: String = "http://localhost:8080/circle/"
  var userInfo: User = null
  var AESKey_FriendList: String = null

  import scala.collection.mutable._

  val friends: Map[String, Boolean] = Map[String, Boolean]()

  override def receive: Receive = {
    case user: User =>
      /* Registration of a user*/
      val keyPair = RSA.generateKeyPair
      val privateKey = Base64.encodeBase64String(keyPair.getPrivate.getEncoded)
      var publicKey = Base64.encodeBase64String(keyPair.getPublic.getEncoded)

      val u = user.copy(key = publicKey)
      val hash = GetDigitalSign.digtialSign(u.toJson.prettyPrint.toString().getBytes(), Main.serverPublicKey)
      val pipeline = addHeader("digitalSignature", hash) ~> addHeader("userId", "-1") ~> sendReceive ~> unmarshal[String]

      val responseFuture = pipeline {
        Post(userParentPath + "register", u)
      }
      val serverAssignedId: String = Await.result(responseFuture, 5 seconds) match {
        case serverAssignedId =>
          serverAssignedId.toString
        case somethingUnexpected =>
          log.warning("The Facebook API call was successful but returned something unexpected: '{}'.", somethingUnexpected)
          "-1"
        case error =>
          log.error(error, "Failure!!!")
          "-1"
      }
      //create friend circle here
      val aes = AES.generateKey
      AESKey_FriendList = aes
      val circleFuture = pipeline {
        Post(circleParentPath + serverAssignedId + "/create", RSA.encrypt1(aes, publicKey))
      }
      userInfo = user.copy(id = serverAssignedId, key = privateKey)
      publicKey = null // available for GC
      sender ! Done
    case SendFriendRequest =>
      if (userInfo == null) {
        println("UserInfo object is null !! Hence the friend request failed")
      }
      else {
        val r = scala.util.Random
        val pipeline = addHeader("userId", userInfo.id) ~> sendReceive ~> unmarshal[String]
        val usersJsonList = Main.userJsonList
        val userList = usersJsonList.convertTo[List[String]]
        val friendId = r.nextInt(userList.size)


        if (userList(friendId) != userInfo.id) {
          val friend = userList(friendId)
          val friendPublicKey = getPublicKey(friend)
          val circleIdsFuture = pipeline {
            Get(circleParentPath + "ids")
          }
          val cricleIds = Await.result(circleIdsFuture, 2 seconds).parseJson.convertTo[List[String]]
          val circleId = cricleIds(0) // friends circle id is always 0
          val publicAESKeyFuture = pipeline {
              Get(circleParentPath + userInfo.id + "/publicKey/" + circleId)
            }
          val publicAESKey = Await.result(publicAESKeyFuture, 2 seconds)
          val AESKeyOfTheCircle = new String(Base64.decodeBase64(RSA.decrypt(publicAESKey, userInfo.key)))
          val aesEncryptedKey = RSA.encrypt1(AESKeyOfTheCircle, friendPublicKey)
          if (friends.get(friend) == None) {
            val responseFutureAddFriend = pipeline {
                Post(userParentPath + userInfo.id + "/SendFriendRequest/" + friend, aesEncryptedKey)
            }
            Await.result(responseFutureAddFriend, 2 seconds)
            friends += (friend -> true)
          }
        }
        else{
          println("Same ids !! No friend request!!")
        }
      }
      sender ! Done
    case AcceptFriendRequest =>
      if (userInfo == null) {
        println("UserInfo object is null !! Hence the accept request failed")
      }
      else {
        val pipeline = addHeader("userId", userInfo.id) ~> sendReceive ~> unmarshal[String]
        val friendRequestsFuture = pipeline {
          Get(userParentPath + "getFriendRequest/" + userInfo.id)
        }
        val friendRequestJson = Await.result(friendRequestsFuture, 4 seconds) match {
          case result => result
        }
        if (!friendRequestJson.isEmpty) {
          val friendRequestList = friendRequestJson.parseJson.convertTo[List[String]]
          val r = scala.util.Random
          if (friendRequestList.size > 0) {
            val friendId = friendRequestList(r.nextInt(friendRequestList.size))

            val friendPublicKey = getPublicKey(friendId).toString
            val circleIdsFuture = pipeline {
              Get(circleParentPath + "ids")
            }
            val cricleIds = Await.result(circleIdsFuture, 2 seconds).parseJson.convertTo[List[String]]
            val circleId = cricleIds(0) // friends circle id is always 0
            val publicAESKeyFuture = pipeline {

                Get(circleParentPath + userInfo.id + "/publicKey/" + circleId)
              }
            val publicAESKey = Await.result(publicAESKeyFuture, 2 seconds)
            val AESKeyOfTheCircle = new String(Base64.decodeBase64(RSA.decrypt(publicAESKey, userInfo.key)))
            val aesKeyOfCircleEncryptedWithFrndPKey = RSA.encrypt1(AESKeyOfTheCircle, friendPublicKey)
            val friendRequestAcceptFuture = pipeline {
              Post(userParentPath + userInfo.id + "/acceptFriendRequest/" + friendId, aesKeyOfCircleEncryptedWithFrndPKey)
            }
            val fr = Await.result(friendRequestAcceptFuture, 2 seconds)
          }
        }
      }
      sender ! Done
    case AddPost =>
      import UserPostJsonImplicits._

      if (userInfo == null) {
        println("UserInfo object is null !! Hence the accept request failed")
      }
      else {
        val post = Main.postList(r.nextInt((Main.postList.size)))
        val (encPost, encodedIv) = AES.encrypt(AESKey_FriendList, post.toJson.toString())
        val encData = EncryptedData(post.id, encodedIv, encPost)

        val pipeline = addHeader("userId", userInfo.id) ~> sendReceive ~> unmarshal[String]
        import EncryptedDataJsonImplicits._
        import spray.httpx.SprayJsonSupport._
        val friendRequestsFuture = pipeline {
          Post(userParentPath + "addPost/" + userInfo.id, encData)
        }
      }
      sender ! Done
    case ReadPost =>
      println
      if (userInfo == null) {
        println("UserInfo object is null !! Hence the accept request failed")
      }
      else {
        val usersJsonList = Main.userJsonList
        val userList = usersJsonList.convertTo[List[String]]
        val friendId = userList(r.nextInt(userList.size))

        println(s"Read Post : From ${userInfo.id} to ${friendId}")

        val pipeline = addHeader("userId", userInfo.id) ~> sendReceive ~> unmarshal[String]
        val friendReadsFuture = pipeline {
          Get(userParentPath + friendId + "/randomPost")
        }
        val fr = Await.result(friendReadsFuture, 5 seconds)
        import EncryptedDataJsonImplicits._
        import spray.json._
        if (fr.contains("{")) {
          var data = fr.parseJson
          val aesEncKey = data.asJsObject.fields("aesKey").convertTo[String]
          val aesKey = RSA.decrypt(aesEncKey, userInfo.key)
          val encEntity = data.asJsObject.fields("encryptedData").convertTo[EncryptedData]
          val decryptedEntity = AES.decrypt(Base64.decodeBase64(aesKey), encEntity.encryptedEntity, encEntity.iv)
          println()
          println(s"The encrypted Post is ${fr}")
          println(s"The decrypted Post is ${new String(Base64.decodeBase64(decryptedEntity))}")
          println()
        }
        else {
          println(fr)
        }
      }
      sender ! Done
    case AddPic =>
      import PictureJsonImplicits._

      if (userInfo == null) {
        println("UserInfo object is null !! Hence the accept request failed")
      }
      else {
        val pic = Main.photosList(r.nextInt((Main.photosList.size)))
        val (encPost, encodedIv) = AES.encrypt(AESKey_FriendList, pic.toJson.toString())
        val encData = EncryptedData(pic.id, encodedIv, encPost)

        val pipeline = addHeader("userId", userInfo.id) ~> sendReceive ~> unmarshal[String]
        import EncryptedDataJsonImplicits._
        import spray.httpx.SprayJsonSupport._
        val friendRequestsFuture = pipeline {
          Post(userParentPath + "addPic/" + userInfo.id, encData)
        }
      }
      sender ! Done
    case ReadPic =>
      println()
      if (userInfo == null) {
        println("UserInfo object is null !! Hence the accept request failed")
      }
      else {
        val usersJsonList = Main.userJsonList
        val userList = usersJsonList.convertTo[List[String]]
        val friendId = userList(r.nextInt(userList.size))

        if (!friendId.equals(userInfo.id)) {

          println(s"Read Pic : From ${userInfo.id} to ${friendId}")

          val pipeline = addHeader("userId", userInfo.id) ~> sendReceive ~> unmarshal[String]
          val friendReadsFuture = pipeline {
            Get(userParentPath + friendId + "/randomPic")
          }
          val fr = Await.result(friendReadsFuture, 5 seconds)
          import EncryptedDataJsonImplicits._
          import spray.json._
          if (fr.contains("{")) {
            var data = fr.parseJson
            val aesEncKey = data.asJsObject.fields("aesKey").convertTo[String]
            val aesKey = RSA.decrypt(aesEncKey, userInfo.key)
            val encEntity = data.asJsObject.fields("encryptedData").convertTo[EncryptedData]
            val decryptedEntity = AES.decrypt(Base64.decodeBase64(aesKey), encEntity.encryptedEntity, encEntity.iv)
            println()
            println(s"The encrypted Post is ${fr}")
            println(s"The decrypted Post is ${new String(Base64.decodeBase64(decryptedEntity))}")
            println()
          }
          else {
            println(fr)
          }
        }
      }
      sender ! Done
  }

  //      sender ! Done
  def getPublicKey(userId: String): String = {
    val pipeline = sendReceive ~> unmarshal[String]
    val publicKeyFuture = pipeline {
      Get(userParentPath + "publicKey/" + userId)
    }
    Await.result(publicKeyFuture, 4 seconds)
  }

}

