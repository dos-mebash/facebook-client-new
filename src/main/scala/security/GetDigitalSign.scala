package security

import java.security.{PrivateKey, MessageDigest, PublicKey}

import org.apache.commons.codec.binary.Base64


/**
  * Created by Ashish on 12/14/2015.
  */
object GetDigitalSign {
  def digtialSign(data:Array[Byte], publicKey: PublicKey):String={
    val md: MessageDigest = MessageDigest.getInstance("SHA-256")
    md.update(data)
    val digest: Array[Byte] = md.digest()
    val hash = RSA.encryptl(digest, publicKey)
    hash
  }
}
