package entity

import spray.json.DefaultJsonProtocol

/**
 * Created by mebin on 12/13/15 for Facebook-client_1
 */

case class User(name: String, id: String, userName: String, password: String, email: String, birthday: String, frnds: Option[List[String]], key: String )


object UserJsonProtocol extends DefaultJsonProtocol {
  implicit val userId = jsonFormat8(User)
}